---
title: 'October’s Monthly Meeting'
type: true
simple-events:
    start: '2020-10-26'
    location: 'Rue Paul Devaux 3, 1000 BXL'
time: '13:00—15:00'
organiser: 'Level Five'
taxonomy:
    category:
        - talk
    tag:
        - Permanent
        - event
text: 'Things have moved on we still have some time till end of the year let''s get things going.'
links:
    'Level Five': 'https://www.levelfivebxl.org'
unpublish_date: '2020-10-26 00:00'
---

