---
title: 'November’s Monthly Meeting'
type: true
simple-events:
    start: '2020-11-23'
    location: 'Rue Paul Devaux 3, 1000 BXL'
time: '13:00—15:00'
organiser: 'Level Five'
taxonomy:
    category:
        - 'monthly meeting'
links:
    'Level Five': levelfivebxl.org
unpublish_date: '2020-11-23 00:00'
---

