---
title: Agenda
content:
    items: '@self.children'
    order:
        by: header.simple-events.start
        dir: asc
    filter:
        published: true
        type: event
---

