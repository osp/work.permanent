---
title: 'Open call - BWMSTR Label'
type: false
simple-events:
    start: '2020-11-08'
    location: bouwmeesterlabel@vlaanderen.be
time: 'deadline 23:59'
taxonomy:
    category:
        - 'open call'
text: 'The BWMSTR is looking for strong spatial concepts and integrated research questions that can make a positive social difference.'
links:
    BWMSTR: 'https://www.vlaamsbouwmeester.be/nl/nieuws/nieuwe-oproep-bouwmeester-label-2020-2021'
external_url: 'https://www.vlaamsbouwmeester.be/nl/nieuws/nieuwe-oproep-bouwmeester-label-2020-2021'
body_classes: ''
order_by: ''
order_manual: ''
unpublish_date: '2020-11-08 00:00'
---

