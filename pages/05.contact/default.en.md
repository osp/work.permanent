---
title: Contact
process:
    markdown: true
    twig: true
cache_enable: false
---

## Contact us

Get in touch, send us a mail to: contact@permanent.org.

## Follow us

Find us on:
[Facebook](https://www.facebook.com/permanentbrussels)
[Twitter](https://twitter.com/permanentbxl)
[Instagram](https://www.instagram.com/permanentbrussels/)

## Join us!

{% include "forms/form.html.twig" with {form: forms('contact-form')} %}

## Partners
	
Permanent is a collaboration between 
[431](http://www.fourthirty-one.org/) 
[Arp:](https://www.arp.works/)
[Dagvorm Cinema](http://www.dagvorm.be)
[Globe Aroma](http://www.globearoma.be/)
[Jubilee](https://jubilee-art.org/)
[Level Five](https://www.levelfivebxl.org/)
[Louiza](https://www.louiza.org/)
[Overtoon](https://www.overtoon.org/)
[Open Source Publishing](http://osp.kitchen/)
[D-E-A-L](http://d-e-a-l.eu/)
with all the individual artists and artworkers temporarily based in the former Actiris-building at the Place de la Bourse in Brussels.

With the support of [Community Land Trust Brussels](https://cltb.be/fr/) and [VUB-weKONEKT.brussels.](https://www.wekonektweek.brussels/en/)

Illustrations: [Jérôme Degive](http://www.picapica.be/). Design and code: [Open Source Publishing](http://osp.kitchen/) and [D-E-A-L](http://d-e-a-l.eu/).
