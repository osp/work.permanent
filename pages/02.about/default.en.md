---
title: About
---

## What is Permanent?

Permanent is a practice-based research with the aim to develop a mixed and shared use
infrastructure for permanently affordable and embedded social and
cultural space in Brussels. By investigating alternative
understandings of ownership and by looking into new legal and
financial models proposed by cooperatives and other “commoning”
initiatives, Permanent wants to develop this much-needed
infrastructure in Brussels without contributing to a process of
gentrification and profit-driven urban development. We are currently
bringing forward the Community Land Trust-model (CLT) as an
alternative. CLT is a social real estate development model in which
the land remains collective property of the community. The model has
already demonstrated its capacity in many different places to include
a larger and more diverse group of people in the development of an
urban living environment beneficial for all. Permanent therefore also
strives for radical solidarity and collaboration with other groups excluded by
speculative urban development schemes. It proposes a building
programme that complements artistic workspaces with public and safe space infrastructure as well as affordable homes for people with
limited means, installing new regimes of solidarity outside of the
own group. This way, Permanent aims to contribute in a pro-active and
sustainable manner to the urban city life in Brussels.

## The campaign

Permanent will develop a public campaign around a couple crucial sites in Brussels. We indicated these sites as exemplary for a
potential anti-speculative, mixed use, and social equitable urban
development. The campaign will put to use artistic practices to undertake interventions in the public space and opinion with the aim to interrupt and question current processes of development. In this way we hope to open up space and time to imagine and experiment with bottom-up alternatives. Paralelel but not detached from this line of action we will develop our knowledge about these complex social, urban, architectural, financial and legal constallations by setting up, hosting and activating a knowledge pool of specialists in each of these fields. With these specialist we will organize several workshops on ownership, legal and financial models in order to formulate some tools that can be used in common. 

## Label

As a project that started from artist cooperative searching for workspaces in Brussels, Permanent will develop a label
that indicates to what extend a specific current occupation of this cooperative reaches the goals
of Permanent. The artist cooperative would like to fulfill the necessary need for qualitative and
affordable workspaces for artists in Brussels. For this the cooperative is dependent on temporary occupations of different
durations in privately and public owned properties. The artists
confident of the positive role it can play in the city but is also
aware of the possible utilization of its organisation in
gentrification and city promotion schemes. To make this apparent
Permanent will develop a label to indicate the measure to which the current
occupations contributes to anti-speculative urban development, to what
extend it applies a mixed use programme in the occupation, and if the
occupation contributes to a more equitable social space. In this way
Permanent serves as a quality label as well as an critical tool to
promote more radical steps in the development of a Permanent location
in Brussels.

## As a working structure

Permanent has a core team that ensures the continuity of the project. In
addition there is a changing constellation of work groups, some for
specific actions others  for developing more long term projects or
enduring tasks. There is a monthly meeting the last monday of the month open to anyone interested.
In these meetings each group gives an update of their progress in the
previous month and different tasks and lines of action are drafted
for the coming month.