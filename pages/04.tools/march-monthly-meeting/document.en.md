---
title: 'March''s monthly meeting'
date: '23-02-2020 11:56'
media_order: 20200320_notes_monthly_meeting.pdf
filter:
    - report
---

Report of the Permanent monthly meeting in March 2020.

Documents:
[.pdf](20200320_notes_monthly_meeting?target=_blank)