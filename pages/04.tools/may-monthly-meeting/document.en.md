---
title: 'May''s monthly meeting'
date: '31-05-2020 12:03'
media_order: 20200525_notes_monthly_meeting.pdf
filter:
    - report
---

Report of the Permanent monthly meeting in May 2020.

Documents:
[.pdf](20200525_notes_monthly_meeting?target=_blank)