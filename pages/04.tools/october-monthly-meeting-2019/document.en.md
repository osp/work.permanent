---
title: 'October''s monthly meeting (2019)'
date: '28-07-2019 11:15'
media_order: '20191028_notes_monthly_meeting.pdf,20191007_notes_monthly_meeting.pdf'
filter:
    - report
---

Report of the first Permanent monthly meeting in October 2019!

Document:
[.pdf](20191007_notes_monthly_meeting?target=_blank)
[.pdf](20191028_notes_monthly_meeting?target=_blank)