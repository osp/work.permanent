---
title: Pitch
media_order: 'permanent-intro-nl.pdf,permanent-intro-fr.pdf'
filter:
    - documentation
---

This is the text we use to pitch Permanent.

Documents:
[.pdf](permanent-intro-nl?target=_blank)
[.pdf](permanent-intro-fr?target=_blank)