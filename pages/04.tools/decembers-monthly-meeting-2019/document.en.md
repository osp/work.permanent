---
title: 'December''s monthly meeting (2019)'
date: '15-12-2019 11:37'
media_order: 20191216_notes_monthly_meeting.pdf
filter:
    - report
---

Report of the Permanent monthly meeting in December 2019.

Documents:
[.pdf](20191216_notes_monthly_meeting?target=_blank)