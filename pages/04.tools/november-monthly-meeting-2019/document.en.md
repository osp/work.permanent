---
title: 'November''s monthly meeting (2019)'
date: '25-11-2019 11:28'
media_order: 20191128_notes_monthly_meeting.pdf
filter:
    - report
---

Report of the Permanent monthly meeting in November 2019.

Documents:
[.pdf](20191128_notes_monthly_meeting?target=_blank)