---
title: 'April''s monthly meeting'
date: '26-04-2020 12:00'
media_order: 20200427_notes_monthly_meeting.pdf
filter:
    - report
---

Report of the Permanent monthly meeting in April 2020.

Documents:
[.pdf](20200427_notes_monthly_meeting?target=_blank)
