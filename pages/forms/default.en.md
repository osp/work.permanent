---
forms:
    contact-form:
        fields:
            firstName:
                label: 'First name'
                autocomplete: 'on'
                type: text
                validate:
                    required: true
            lastName:
                label: 'Last name'
                autocomplete: 'on'
                type: text
                validate:
                    required: true
            email:
                label: Email
                type: email
                validate:
                    required: true
            phone:
                label: 'Phone number'
                type: tel
            type:
                label: 'Who are you?'
                type: checkboxes
                default:
                    option1: false
                    option2: false
                options:
                    option1: 'an individual (pioneer)'
                    option2: 'an organisation (partner)'
            engagement:
                label: 'How do you want to be engaged?'
                type: checkboxes
                default:
                    option1: false
                    option2: false
                    option3: false
                    option4: false
                    option5: false
                options:
                    option1: 'Stay up to date'
                    option2: 'Assist to monthly meeting'
                    option3: 'Join a workgroup'
                    option4: 'Become a partner'
                    option5: 'Support in another way'
            message:
                label: 'What can you contribute?'
                type: textarea
                size: large
        buttons:
            submit:
                type: submit
                value: Submit
            reset:
                type: reset
                value: Reset
        process:
            save:
                fileprefix: contact-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
            email:
                subject: '[Contact Permanent] {{ form.value.firstName|e }} {{ form.value.lastName|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
            message: 'Thank you for getting in touch!'
            display: /thankyou
---

