// add your script here
const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

filterSelection("all")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filter");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    RemoveClass(x[i], "visible");
    if (x[i].className.indexOf(c) > -1) AddClass(x[i], "visible");
    getActveBtn();
  }
}

function AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

function RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

function getActveBtn() {
  let btnContainer = $(".filter_container");
  if (btnContainer) {
    let btns = btnContainer.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
        let current = $(".btn.active");
        current.classList.remove('active');
        this.className += " active";
      });
    }
  };
}; 

function menu(){
  let body = $('#body')
    , header = $('.header')
    , headerBtn = $('.header__btn');

  function menuCta(){
    if (header.classList.contains('close')) {
      $('#top').classList.add('no-overflow');
      header.className = "header open";
      headerBtn.className = "header__btn active";
      headerBtn.innerHTML = 'close';
    } else {
      closeMenu();
    }
  }

  function closeMenu(){
    if (!header.classList.contains('close')) {
      $('#top').classList.remove('no-overflow');
      header.className = "header close";
      headerBtn.className = "header__btn";
      headerBtn.innerHTML = 'menu';
    };
  }

  // body.addEventListener("click", closeMenu);
  body.addEventListener('mouseenter', closeMenu);
  headerBtn.addEventListener("click", menuCta);
}

menu();

/*random*/
function randomHeader() {
    var target = $$('.header a');
    var classes = [
        'sample-branche',
        'sample-fleur-blanche',
        'sample-fleur-jaune',
        'sample-fleur-rouge',
        'sample-fleur-violettes',
        'sample-fougere',
        'sample-pousse1',
        'sample-pousse2',
        'sample-pousse3',
              ];
  if (target) {
    for (var i = 0; i < target.length; i++) {
      var randomNum = Math.floor( Math.random() * classes.length );
      var result = classes[ randomNum ];
      target[i].classList.add(result);
    }
  }
};

function randomIllustration() {
  // if ($('body.contact')) {
    var target = $$('.wrapper a');
    var classes = [
        'sample-branche',
        'sample-fleur-blanche',
        'sample-fleur-jaune',
        'sample-fleur-rouge',
        'sample-fleur-violettes',
        'sample-fougere',
        'sample-pousse1',
        'sample-pousse2',
        'sample-pousse3',
              ];
  // };
  if ($('body.agenda')) {
    var target = $$("div:nth-of-type(2n) .event_content_wrapper");
    var classes = [
        'corniche_face',
        'corniche_profil',
        // 'fleur_jaune',
        // 'fleur_orange',
        'fleur_violette',
        'fleur_paquerette',
        'plante_blanche1',
        'plante_blanche2',
        // 'plante_epilobe1',
        'plante_epilobe2',
        'plante_epilobe3',
        'plante_epilobe4',
        'plante_feuille',
        'plante_fougere',
        'plante_haute',
        'plante_plate',
        'plante_rouge'
              ];
  };
  if (target) {
    for (var i = 0; i < target.length; i++) {
      var randomNum = Math.floor( Math.random() * classes.length );
      var result = classes[ randomNum ];
      target[i].classList.add(result);
    }
  }
};

randomHeader();
randomIllustration();